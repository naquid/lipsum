package ai.protoaone.lipsum.configuration;
// package ai.protoaone.lipsum.configuration;

// import java.util.Collections;

// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
// import org.springframework.context.support.ResourceBundleMessageSource;
// import org.thymeleaf.TemplateEngine;
// import org.thymeleaf.spring6.SpringTemplateEngine;
// import org.thymeleaf.templatemode.TemplateMode;
// import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
// import org.thymeleaf.templateresolver.ITemplateResolver;

// @Configuration
// public class ThymeleafMailConfiguration {//extends  ApplicationContextAware, EnvironmentAware

//     private static final String EMAIL_TEMPLATE_ENCODING = "UTF-8";

//     @Bean
//     public TemplateEngine templateEngine() {
//         final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
//         templateEngine.addTemplateResolver(textTemplateResolver());
//         templateEngine.addTemplateResolver(htmlTemplateResolver());
//         templateEngine.addTemplateResolver(stringTemplateResolver());
//         templateEngine.setTemplateEngineMessageSource(messageSource());
//         return templateEngine;
//     }

//     @Bean
//     public ResourceBundleMessageSource messageSource() {
//         final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
//         messageSource.setBasename("mail/MailMessages");
//         return messageSource;
//     }

//     private ITemplateResolver textTemplateResolver() {
//         final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
//         templateResolver.setOrder(1);
//         templateResolver.setResolvablePatterns(Collections.singleton("text/*"));
//         templateResolver.setPrefix("/mail/");
//         templateResolver.setSuffix(".txt");
//         templateResolver.setTemplateMode(TemplateMode.TEXT);
//         templateResolver.setCharacterEncoding(EMAIL_TEMPLATE_ENCODING);
//         templateResolver.setCacheable(false);
//         return templateResolver;
//     }

//     private ITemplateResolver htmlTemplateResolver() {
//         final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
//         templateResolver.setOrder(2);
//         templateResolver.setResolvablePatterns(Collections.singleton("html/*"));
//         templateResolver.setPrefix("/mail");
//         templateResolver.setSuffix(".html");
//         templateResolver.setTemplateMode(TemplateMode.HTML);
//         templateResolver.setCharacterEncoding(EMAIL_TEMPLATE_ENCODING);
//         templateResolver.setCacheable(false);
//         return templateResolver;
//     }

//     private ITemplateResolver stringTemplateResolver() {
//         final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
//         templateResolver.setOrder(3);
//         templateResolver.setTemplateMode(TemplateMode.HTML);
//         templateResolver.setCacheable(false);
//         return templateResolver;
//     }
// }
