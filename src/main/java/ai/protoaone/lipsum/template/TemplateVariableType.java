package ai.protoaone.lipsum.template;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TemplateVariableType {
    IMAGE("IMAGE"),
    TEXT("TEXT")
    ;
    private String name;
}
