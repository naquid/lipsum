package ai.protoaone.lipsum.template;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class TemplateService {
    private static final Logger logger = LoggerFactory.getLogger(TemplateService.class);

    @Autowired
    private TemplateEngine templateEngine;

    // @Autowired
    // private JavaMailSender mailSender;

    public String fillTemplate(TemplateDto dto) {
        logger.info("Filling Template Initialized");
        final Context context = new Context();
        context.setVariables(dto.getMap());
        final String text = templateEngine.process(dto.getName(), context);
        logger.info("Filling Template Completed");
        return text;
    }

    public void sendMail(String templateString) {
        
    }
}
