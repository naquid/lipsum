package ai.protoaone.lipsum.template;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class TemplateVariableDto {
    private String name;
    private Object value;
    @Builder.Default
    private TemplateVariableType type = TemplateVariableType.TEXT;
}
