package ai.protoaone.lipsum.template;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
public class TemplateController {
    private static final Logger logger = LoggerFactory.getLogger(TemplateController.class);

    @Autowired
    private TemplateService templateService;

    @PostMapping("/template/fill")
    public String fillTemplate(@RequestBody TemplateDto dto) {
        logger.info("Filling Template Initialized");
        String filledTemplate = templateService.fillTemplate(dto);    
        logger.info("Filling Template Completed");
        return filledTemplate;
    }
}
