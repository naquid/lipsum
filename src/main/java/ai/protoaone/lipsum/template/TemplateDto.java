package ai.protoaone.lipsum.template;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TemplateDto {
    private static final Logger logger = LoggerFactory.getLogger(TemplateDto.class);
    private String id;
    private String name;
    List<TemplateVariableDto> variables;

    public Map<String,Object> getMap() {
        Map<String,Object> variableMap = this.getVariables().stream()
        .collect(
            Collectors.toMap(
                o->o.getName(),
                o->o.getValue()
            )
        );
        logger.info("variable map:{}",variableMap);
        return variableMap;
    }
}
